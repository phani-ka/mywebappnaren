FROM tomcat
ADD sample.war /usr/local/tomcat/webapps/
VOLUME /usr/local/tomcat
EXPOSE 8080
CMD ["catalina.sh", "run"]
